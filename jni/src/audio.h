#ifndef TEST1H
#define TEST1H
extern int breakPlay; // 声明全局变量使播放停止，现已废除，使用quit
extern int isPlaying; // 检测是否在播放
extern int quit; //用户主动退出

int startPlay(int argc, char *argv[]);
void stopPlay();
int checkPlaying();
#endif
