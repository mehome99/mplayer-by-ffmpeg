/*
    SDL_android_main.c, placed in the public domain by Sam Lantinga  3/13/14
*/
#include "../SDL/src/SDL_internal.h"

#ifdef __ANDROID__

/* Include the SDL main definition header */
#include "SDL_main.h"
#include "audio.h"
/*******************************************************************************
                 Functions called by JNI
*******************************************************************************/
#include <jni.h>
#include <string.h>

/* Called before SDL_main() to initialize JNI bindings in SDL library */
extern void SDL_Android_Init(JNIEnv* env, jclass cls);

/* Start up the SDL app */
jint Java_cn_kapple_mplayer_Mplayer_nativeCheckPlaying(JNIEnv* env, jclass cls, jobject obj)
{
	return (jint)checkPlaying();
}

void Java_cn_kapple_mplayer_Mplayer_nativeFinal(JNIEnv* env, jclass cls, jobject obj)
{
	stopPlay();
}

char* strins(char* dest, const char* src, int pos)
{
    int len = strlen(src);
    for (int i = strlen(dest); i >= pos; i--){
    	dest[i + len] = dest[i];    // ͬʱҲ�����ַ���������
    	//printf("Character where strings differ is at position %d\n", 1);
    }

   for (int j = pos; j < pos + len; j++)
        dest[j] = src[j - pos];
    return dest;
}

void Java_cn_kapple_mplayer_Mplayer_nativeInit(JNIEnv* env, jclass cls, jobject obj)
{
    /* This interface could expand with ABI negotiation, calbacks, etc. */
    SDL_Android_Init(env, cls);

    SDL_SetMainReady();

    /* Run the application code! */
    int status;
    char *argv[2];
    argv[0] = SDL_strdup("SDL_app");
    //argv[1] = NULL;
    //argv[1] = "mmsh://alive.rbc.cn/am774";
    //argv[1] = "mmsh://alive.rbc.cn/fm974";
    const char* str;
    char str1[128];
       str = (*env)->GetStringUTFChars(env,obj,0);
       if(str != NULL) {
    	   //strcpy(argv[1],str);
    	   strcpy(str1,str);
    	   argv[1] = str1; /* OutOfMemoryError already thrown */
       }else{
    	  argv[1] = "mmsh://alive.rbc.cn/am774";
       }
    char pos[5];
    strncpy(pos,argv[1],4);
    pos[4]='\0';
    //strupr(pos);
    char* pos1="mms:";
    if(strcmp(pos,pos1)==0){
    	strcpy(str1,argv[1]);
    	strins(str1,"h",3);
    	argv[1]=str1;
    }
    //pos=stringchar();
    //status = SDL_main(1, argv);
    int i=0;
    while (i<6){
    	i++;
    	status = SDL_main(1, argv);
    	if ((isPlaying==1) || (quit=1)){
    		break;
    	}
    }
    //status = startPlay(1,argv);
    //SDL_Delay(40000);
    //stopPlay();
    /* Do not issue an exit or the whole application will terminate instead of just the SDL thread */
    /* exit(status); */
}

#endif /* __ANDROID__ */

/* vi: set ts=4 sw=4 expandtab: */
