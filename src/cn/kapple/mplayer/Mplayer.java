package cn.kapple.mplayer;

import org.libsdl.app.SDLActivity;

import android.os.Bundle;
import android.util.Log;

public class Mplayer extends SDLActivity {
	// Load the .so
	static {
		System.loadLibrary("test");
		System.loadLibrary("avutil-52");
		System.loadLibrary("avcodec-55");
		System.loadLibrary("swresample-0");
		System.loadLibrary("avformat-55");
		System.loadLibrary("avresample-1");
		System.loadLibrary("postproc-52");
		System.loadLibrary("swscale-2");
		System.loadLibrary("avfilter-4");
		System.loadLibrary("SDL2");
		// System.loadLibrary("SDL2_image");
		// System.loadLibrary("SDL2_mixer");
		// System.loadLibrary("SDL2_net");
		// System.loadLibrary("SDL2_ttf");
		System.loadLibrary("main");
	}

	public static native void nativeInit(String s);

	public static native void nativeFinal();

	public static native int nativeCheckPlaying();

	public static SDLActivity mp = null;

	public boolean isPlaying(){
		if (Mplayer.nativeCheckPlaying()==0) {
			return false;
		}else{
			return true;
		}
	}
	
	public static void open(String aPath) {
		playPath = aPath;
		if (mp == null) {
			mp = new Mplayer();
		}
		if (Mplayer.nativeCheckPlaying()==0) {
			SDLActivity.mSDLThread = new Thread(new SDLMain(), "SDLThread");
			SDLActivity.mSDLThread.start();
		}
	}

	public static void stop() {
		if (Mplayer.nativeCheckPlaying()==1) {
			Mplayer.nativeFinal();
		}
		// Thread trd;
		// trd=SDLActivity.mSDLThread;
		// SDLActivity.mSDLThread = new Thread(new SDLMain(), "SDLThread");
		// SDLActivity.mSDLThread.start();
		// trd.stop();
	}

	public Mplayer(){
		mp = this;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.v("SDLMplayer", "onCreate():" + mSingleton);
		super.onCreate(savedInstanceState);
		mp = this;

	}
}

class SDLMain implements Runnable {
	@Override
	public void run() {
		// Runs SDL_main()
		// SDLActivity.nativeInit("mmsh://alive.rbc.cn/fm974");
		Mplayer.nativeInit(SDLActivity.playPath);
		// Log.v("SDL", "SDL thread terminated");
	}

}